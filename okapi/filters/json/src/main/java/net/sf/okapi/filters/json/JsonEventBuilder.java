/*===========================================================================
  Copyright (C) 2009-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.json;

import net.sf.okapi.common.filters.EventBuilder;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.InlineCodeFinder;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextFragment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonEventBuilder extends EventBuilder {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private InlineCodeFinder codeFinder;

	public JsonEventBuilder(String rootId, IFilter subFilter) {
		super(rootId, subFilter);
		codeFinder = null;
	}
	
	@Override
	protected ITextUnit postProcessTextUnit(ITextUnit textUnit) {
		TextFragment text = textUnit.getSource().getFirstContent();
		String unescaped = unescape(text);
		text.setCodedText(unescaped);
		
		if ( codeFinder != null ) {
			codeFinder.process(text);
		}
		return textUnit;
	}
	
	public String decode(String value) {
		return unescape(new TextFragment(value));
	}
	
	private String unescape(TextFragment text) {
		StringBuilder unescaped = new StringBuilder();
		char ch;
		for (int i = 0; i < text.length(); i++) {
			ch = text.charAt(i);
			switch(ch) {
			case '\\':
				break;
			default:
				unescaped.append(ch);
				continue;
			}
						
			// previous char was '\'
			ch = text.charAt(++i);
			switch (ch) {				
			case 'b':
				unescaped.append('\b');
				break;
			case 'f':
				unescaped.append('\f');
				break;
			case 'n':
				unescaped.append('\n');
				break;
			case 'r':
				unescaped.append('\r');
				break;
			case 't':
				unescaped.append('\t');
				break;
			case '\\':
			case '/':
			case '"':
				unescaped.append(ch);
				break;
			default: // Unexpected escape sequence
				logger.warn("Unexpected Json escape sequence '\\{}'.", ch);
				unescaped.append('\\');
				unescaped.append(ch);
				break;
			}
		}

		return unescaped.toString();
	}
	
	public void setCodeFinder(InlineCodeFinder codeFinder) {
		this.codeFinder = codeFinder;
	}
}
