/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/
package net.sf.okapi.filters.openxml;

import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * Code to parse the [Content_Types].xml files present in Office OpenXML documents.
 */
public class ContentTypes {

	static final String CONTENT_TYPES_NS = "http://schemas.openxmlformats.org/package/2006/content-types";
	static final QName DEFAULT = new QName(CONTENT_TYPES_NS, "Default");
	static final QName OVERRIDE = new QName(CONTENT_TYPES_NS, "Override");
	static final QName PARTNAME_ATTR = new QName("PartName");
	static final QName CONTENTTYPE_ATTR = new QName("ContentType");
	static final QName EXTENSION_ATTR = new QName("Extension");
	
	private XMLInputFactory factory;
	private Map<String, String> defaults = new HashMap<String, String>();
	private Map<String, String> overrides = new HashMap<String, String>();
	
	public ContentTypes(XMLInputFactory factory) {
		this.factory = factory;
	}
	
	public String getContentType(String partName) {
		partName = ensureWellformedPath(partName);
		if (overrides.containsKey(partName)) {
			return overrides.get(partName);
		}
		String suffix = getSuffix(partName);
		if (defaults.containsKey(suffix)) {
			return defaults.get(suffix);
		}
		
		// Unknown file - this shouldn't ever happen.  We 
		// report this as arbitrary data.
		return "application/octet-stream";
	}
	
	public void parseFromXML(Reader reader) throws XMLStreamException {
		XMLEventReader eventReader = factory.createXMLEventReader(reader);
		
		while (eventReader.hasNext()) {
			XMLEvent e = eventReader.nextEvent();
			
			if (e.isStartElement()) {
				StartElement el = e.asStartElement();
				if (el.getName().equals(DEFAULT)) {
					Attribute ext = el.getAttributeByName(EXTENSION_ATTR);
					Attribute type = el.getAttributeByName(CONTENTTYPE_ATTR);
					if (ext != null && type != null) {
						defaults.put(ext.getValue(), type.getValue());
					}
				}
				else if (el.getName().equals(OVERRIDE)) {
					Attribute part = el.getAttributeByName(PARTNAME_ATTR);
					Attribute type = el.getAttributeByName(CONTENTTYPE_ATTR);
					if (part != null && type != null) {
						overrides.put(ensureWellformedPath(part.getValue()),
									  type.getValue());
					}
				}
			}
		}
	}
	
	private String getSuffix(String partName) {
		String suffix = partName;
		int i = suffix.lastIndexOf('.');
		if (i != -1) {
			suffix = suffix.substring(i + 1);
		}
		return suffix;
	}
	
	private String ensureWellformedPath(String p) {
		return p.startsWith("/") ? p : "/" + p;
	}
}
