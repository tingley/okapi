package net.sf.okapi.tkit.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.XMLFileCompare;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filters.InputDocument;
import net.sf.okapi.common.filters.RoundTripComparison;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.xliff.Parameters;
import net.sf.okapi.filters.xliff.XLIFFFilter;
import net.sf.okapi.filters.xliff.XLIFFFilterTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripXliffSkeletonBasedTkitsIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private XLIFFFilter xliffFilter;
	private String[] testFileList;
	private String root;

	@Before
	public void setUp() throws Exception {
		xliffFilter = new XLIFFFilter();
		testFileList = getXliffFiles();
		URL url = RoundTripXliffSkeletonBasedTkitsIT.class.getResource("/xliff/lqiTest.xlf");
		root = Util.getDirectoryName(url.toURI().getPath()) + File.separator;
	}

	@After
	public void tearDown() throws Exception {
		xliffFilter.close();
	}

	@Test
	public void roundTripXliffFiles() throws FileNotFoundException {
		runTest(false);
		runTest(true);
	}
	
	@SuppressWarnings("resource")
    private void runTest(boolean segment) throws FileNotFoundException {
		for (String f : testFileList) {
			if ("lqiTest.xlf".equals(f)) continue; // Issue 388
			LOGGER.trace(f);
			String xliff = root+f+".xliff";
			String original = root + f;
			String tkitMerged = root+f+".tkitMerged";
			String merged = root+f+".merged";			
			RawDocument rd = new RawDocument(Util.toURI(original), "utf-8", LocaleId.ENGLISH, LocaleId.SPANISH);
			List<Event> events = FilterTestDriver.getEvents(xliffFilter, rd, null);
			if (segment) {
				events = RoundTripUtils.segment(events);
			}
			RoundTripUtils.writeXliff(events, root, xliff);
			String configId = xliffFilter.getConfigurations().get(0).configId;
			RoundTripUtils.tkitMerge(original, xliff, tkitMerged, configId);
			RoundTripUtils.legacyMerge(original, xliff, merged, configId);
			XMLFileCompare compare = new XMLFileCompare();
			// ImplementationPlan.docx.xlf has wrong inline code id's
			if (!f.equals("ImplementationPlan.docx.xlf")) {
				assertTrue("Xliff Merged Compare: " + f, compare.compareFilesPerLines(tkitMerged, merged));
			}
			
			RawDocument ord = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH, LocaleId.SPANISH);
			RawDocument trd = new RawDocument(Util.toURI(tkitMerged), "UTF-8", LocaleId.ENGLISH, LocaleId.SPANISH);
			List<Event> o = FilterTestDriver.getEvents(xliffFilter, ord, null);
			List<Event> t = FilterTestDriver.getEvents(xliffFilter, trd, null);
			assertTrue("Compare original and merged: " + f, FilterTestDriver.compareEvents(o, t, true, true, false, false));	
			
			rd.close();
		}
	}
	
	@Test
	public void testMalformedTagsAfterMerge() throws URISyntaxException, FileNotFoundException {		
		String f = "error_invalid_xml_on_merge.xlf";
		LOGGER.trace(f);
		String xliff = root+f+".xliff";
		String original = root + f;
		String tkitMerged = root+f+".tkitMerged";
		String merged = root+f+".merged";			

		XLIFFFilter xf = new XLIFFFilter();
		Parameters p = (Parameters) xf.getParameters();
		URL url = XLIFFFilterTest.class.getResource("/xliff/okf_xliff@malformed-tags-error.fprm");
		p.load(url.toURI(), false);
		xf.setParameters(p);

		RawDocument rd = new RawDocument(Util.toURI(original), "utf-8", LocaleId.fromString("en-us"), LocaleId.fromString("de-de"));
		List<Event> events = FilterTestDriver.getEvents(xf, rd, p);
		events = RoundTripUtils.segment(events, LocaleId.fromString("en-us"), LocaleId.fromString("de-de"));
		RoundTripUtils.writeXliff(LocaleId.fromString("de-de"), events, root, xliff);
		String configId = xf.getConfigurations().get(0).configId;
		RoundTripUtils.merge(LocaleId.fromString("en-us"), LocaleId.fromString("de-de"), false, original, xliff, tkitMerged, configId, null);
		RoundTripUtils.merge(LocaleId.fromString("en-us"), LocaleId.fromString("de-de"), true, original, xliff, merged, configId, null);
		XMLFileCompare compare = new XMLFileCompare();			
		assertTrue(compare.compareFilesPerLines(tkitMerged, merged));
		
		@SuppressWarnings("resource")
		RawDocument ord = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.fromString("en-us"), LocaleId.fromString("de-de"));
		@SuppressWarnings("resource")
		RawDocument trd = new RawDocument(Util.toURI(tkitMerged), "UTF-8", LocaleId.fromString("en-us"), LocaleId.fromString("de-de"));
		List<Event> o = FilterTestDriver.getEvents(xf, ord, p);
		List<Event> t = FilterTestDriver.getEvents(xf, trd, p);
		assertTrue("Compare original and merged: " + f, FilterTestDriver.compareEvents(o, t, true));	
		
		rd.close();
	}

	private static String[] getXliffFiles() throws URISyntaxException {
		// read all files in the test xml directory
		URL url = RoundTripXliffSkeletonBasedTkitsIT.class.getResource("/xliff/lqiTest.xlf");
		File dir = new File(url.toURI()).getParentFile();

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".xlf");
			}
		};
		return dir.list(filter);
	}
}
