package net.sf.okapi.tkit.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.html.HtmlFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripHtmlSkeletonBasedTkitsIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private HtmlFilter htmlFilter;
	private String[] testFileList;
	private String root;

	@Before
	public void setUp() throws Exception {
		htmlFilter = new HtmlFilter();
		htmlFilter.setParametersFromURL(HtmlFilter.class.getResource("nonwellformedConfiguration.yml"));
		testFileList = getHtmlTestFiles();
		URL url = RoundTripHtmlSkeletonBasedTkitsIT.class.getResource("/html/324.html");
		root = Util.getDirectoryName(url.toURI().getPath()) + File.separator;
	}

	@After
	public void tearDown() throws Exception {
		htmlFilter.close();
	}

	@Test
	public void roundTripHtml() throws FileNotFoundException {
		runTest(false);
		runTest(true);
	}
	
	@SuppressWarnings("resource")
    private void runTest(boolean segment) throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);
			String xliff = root+f+".xliff";
			String original = root + f;
			String tkitMerged = root+f+".tkitMerged";
			String merged = root+f+".merged";
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			
			List<Event> events = FilterTestDriver.getEvents(htmlFilter, rd, null);
			if (segment) {
				events = RoundTripUtils.segment(events);
			}
			
			RoundTripUtils.writeXliff(events, root, xliff);
						
			String configId = htmlFilter.getConfiguration(htmlFilter.getName()).configId;
	        RoundTripUtils.tkitMerge(original, xliff, tkitMerged, configId);
			RoundTripUtils.legacyMerge(original, xliff, merged, configId);
			
			FileCompare compare = new FileCompare();
			assertTrue(compare.compareFilesPerLines(tkitMerged, merged, rd.getEncoding()));
			rd.close();
			
			RawDocument ord = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			RawDocument trd = new RawDocument(Util.toURI(tkitMerged), "UTF-8", LocaleId.ENGLISH);
			List<Event> o = FilterTestDriver.removeEmptyTextUnits(FilterTestDriver.getEvents(htmlFilter, ord, null));
			List<Event> t = FilterTestDriver.removeEmptyTextUnits(FilterTestDriver.getEvents(htmlFilter, trd, null));
			assertTrue("Compare original and merged: " + f, FilterTestDriver.compareEvents(o, t, true, true, false, false));						
		}
	}

	private static String[] getHtmlTestFiles() throws URISyntaxException {
		// read all files in the test html directory
		URL url = RoundTripHtmlSkeletonBasedTkitsIT.class
				.getResource("/html/324.html");
		File dir = new File(url.toURI()).getParentFile();

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".html") || name.endsWith(".htm")
						|| name.endsWith(".phtml");
			}
		};
		return dir.list(filter);
	}
}
