package net.sf.okapi.tkit.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.tmx.TmxFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripTmxSkeletonBasedTkitsIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private TmxFilter tmxFilter;
	private String[] testFileList;
	private String root;

	@Before
	public void setUp() throws Exception {
		tmxFilter = new TmxFilter();
		testFileList = getFiles();
		URL url = RoundTripTmxSkeletonBasedTkitsIT.class.getResource("/tmx/sampleTMX2.tmx");
		root = Util.getDirectoryName(url.toURI().getPath()) + File.separator;
	}

	@After
	public void tearDown() throws Exception {
		tmxFilter.close();
	}

	@SuppressWarnings("resource")
    @Test
	public void roundTripTmxFiles() throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);
			String xliff = root+f+".xliff";
			String original = root + f;
			String tkitMerged = root+f+".tkitMerged";
			String merged = root+f+".merged";
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.fromString("en-US"), LocaleId.FRENCH);
			List<Event> events = FilterTestDriver.getEvents(tmxFilter, rd, null);
			RoundTripUtils.writeXliff(LocaleId.FRENCH, events, root, xliff);		
			String configId = tmxFilter.getConfigurations().get(0).configId;
			RoundTripUtils.tkitMerge(original, xliff, tkitMerged, configId);
			RoundTripUtils.legacyMerge(original, xliff, merged, configId);
			FileCompare compare = new FileCompare();
			// FIXME bugs in legacy merger!!
			//[main] TRACE net.sf.okapi.common.FileCompare -  out: "    <seg><it x="1" pos="begin" type="italic">&lt;I></it><bpt x="2" i="1" type="bold">&lt;B></bpt>Cliquez <ph x="3" type="image" assoc="b">&lt;IMG SRC="here.png" ALT="<sub>Commencez ici !</sub>"></ph> pour <hi x="4" type="verb">commencer</hi>.<ept i="1">&lt;/B></ept></seg>"
			//[main] TRACE net.sf.okapi.common.FileCompare - gold: "    <seg><it x="1" pos="begin" type="italic">&lt;I></it><bpt x="2" i="1" type="bold">&lt;B></bpt>Cliquez <ph x="3" type="image" assoc="b">&lt;IMG SRC="here.png" ALT="<sub>Start Here!</sub>"></ph> pour <hi x="4" type="verb">commencer</hi>.<ept i="1">&lt;/B></ept></seg>"
			//assertTrue(compare.compareFilesPerLines(tkitMerged, merged, rd.getEncoding()));
			
			RawDocument ord = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.fromString("en-US"), LocaleId.FRENCH);
			RawDocument trd = new RawDocument(Util.toURI(tkitMerged), "UTF-8", LocaleId.fromString("en-US"), LocaleId.FRENCH);
			List<Event> o = FilterTestDriver.getEvents(tmxFilter, ord, null);
			List<Event> t = FilterTestDriver.getEvents(tmxFilter, trd, null);
			assertTrue("Compare original and merged: " + f, FilterTestDriver.compareEvents(o, t, true, false, true, false));
		}
	}
	
	@SuppressWarnings("resource")
    @Test
    public void single() throws FileNotFoundException, URISyntaxException {
        URL url = RoundTripTmxSkeletonBasedTkitsIT.class.getResource("/tmx/a_small_test2.tmx");
        String f = new File(url.toURI()).getName();
        LOGGER.trace(f);
        String xliff = root+f+".xliff";
        String original = root + f;
        String tkitMerged = root+f+".tkitMerged";
        String merged = root+f+".merged";
        RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.fromString("en-US"), LocaleId.FRENCH);
        List<Event> events = FilterTestDriver.getEvents(tmxFilter, rd, null);
        RoundTripUtils.writeXliff(LocaleId.FRENCH, events, root, xliff);     
        String configId = tmxFilter.getConfigurations().get(0).configId;
        RoundTripUtils.tkitMerge(original, xliff, tkitMerged, configId);
        RoundTripUtils.legacyMerge(original, xliff, merged, configId);
        FileCompare compare = new FileCompare();
        // FIXME bugs in legacy merger!!
        //[main] TRACE net.sf.okapi.common.FileCompare -  out: "    <seg><it x="1" pos="begin" type="italic">&lt;I></it><bpt x="2" i="1" type="bold">&lt;B></bpt>Cliquez <ph x="3" type="image" assoc="b">&lt;IMG SRC="here.png" ALT="<sub>Commencez ici !</sub>"></ph> pour <hi x="4" type="verb">commencer</hi>.<ept i="1">&lt;/B></ept></seg>"
        //[main] TRACE net.sf.okapi.common.FileCompare - gold: "    <seg><it x="1" pos="begin" type="italic">&lt;I></it><bpt x="2" i="1" type="bold">&lt;B></bpt>Cliquez <ph x="3" type="image" assoc="b">&lt;IMG SRC="here.png" ALT="<sub>Start Here!</sub>"></ph> pour <hi x="4" type="verb">commencer</hi>.<ept i="1">&lt;/B></ept></seg>"
        //assertTrue(compare.compareFilesPerLines(tkitMerged, merged, rd.getEncoding()));
        
        RawDocument ord = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.fromString("en-US"), LocaleId.FRENCH);
        RawDocument trd = new RawDocument(Util.toURI(tkitMerged), "UTF-8", LocaleId.fromString("en-US"), LocaleId.FRENCH);
        List<Event> o = FilterTestDriver.getEvents(tmxFilter, ord, null);
        List<Event> t = FilterTestDriver.getEvents(tmxFilter, trd, null);
        assertTrue("Compare original and merged: " + f, FilterTestDriver.compareEvents(o, t, true, false, true, false));
    }

	private static String[] getFiles() throws URISyntaxException {
		URL url = RoundTripTmxSkeletonBasedTkitsIT.class.getResource("/tmx/sampleTMX2.tmx");
		File dir = new File(url.toURI()).getParentFile();

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".tmx");
			}
		};
		return dir.list(filter);
	}
}
