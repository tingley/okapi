package net.sf.okapi.tkit.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.MimeTypeMapper;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.ZipFileCompare;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.archive.ArchiveFilter;
import net.sf.okapi.filters.archive.Parameters;
import net.sf.okapi.filters.tmx.TmxFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripArchiveSkeletonBasedTkitsIT {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private ArchiveFilter archiveFilter;
	private String[] testFileList;
	private String root;

	@Before
	public void setUp() throws Exception {
		FilterConfigurationMapper fcm = new FilterConfigurationMapper();
		// Create configuration for tmx extension (if we need text units from
		// tmx as well)
		fcm.addConfiguration(new FilterConfiguration(
				"okf_tmx",
				MimeTypeMapper.TMX_MIME_TYPE,
				TmxFilter.class.getName(),
				// "net.sf.okapi.filters.tmx.TmxFilter",
				"TMX",
				"Configuration for Translation Memory eXchange (TMX) documents.",
				null, ".tmx;"));

		archiveFilter = new ArchiveFilter();
		archiveFilter.setFilterConfigurationMapper(fcm);

		Parameters params = new Parameters();
		params.setFileNames("*.xliff2, *.tmx, *02.xlf");
		params.setConfigIds("okf_xliff, okf_tmx, okf_xliff");
		archiveFilter.setParameters(params);

		testFileList = getArchiveFiles();
		URL url = RoundTripArchiveSkeletonBasedTkitsIT.class.getResource("/archive/test1_es.archive");
		root = Util.getDirectoryName(url.toURI().getPath()) + File.separator;
	}

	@After
	public void tearDown() throws Exception {
		archiveFilter.close();
	}

	@SuppressWarnings("resource")
    @Test
	public void roundTripArchiveFiles() throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);
			String xliff = root + f + ".xliff";
			String original = root + f;
			String tkitMerged = root + f + ".tkitMerged";
			String merged = root + f + ".merged";
			@SuppressWarnings("resource")
			// closed in getEvents
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.fromString("en-US"), LocaleId.fromString("fr-FR"));
			List<Event> events = FilterTestDriver.getEvents(archiveFilter, rd, null);
			RoundTripUtils.writeXliff(LocaleId.fromString("fr-FR"), events, root, xliff);			
			String configId = archiveFilter.getConfigurations().get(0).configId;	        
			RoundTripUtils.legacyMerge(original, xliff, merged, configId);
			RoundTripUtils.tkitMerge(original, xliff, tkitMerged, configId);
			ZipFileCompare compare = new ZipFileCompare();
			assertTrue(compare.compareFilesPerLines(tkitMerged, merged, rd.getEncoding()));			
            
            RawDocument ord = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.fromString("en-US"), LocaleId.fromString("fr-FR"));
            RawDocument trd = new RawDocument(Util.toURI(tkitMerged), "UTF-8", LocaleId.fromString("en-US"), LocaleId.fromString("fr-FR"));
            List<Event> o = FilterTestDriver.getEvents(archiveFilter, ord, null);
            List<Event> t = FilterTestDriver.getEvents(archiveFilter, trd, null);
            assertTrue("Compare original and merged: " + f, FilterTestDriver.compareEvents(o, t, false, true, false, true));
		}
	}

	private static String[] getArchiveFiles() throws URISyntaxException {
		// read all files in the test xml directory
		URL url = RoundTripArchiveSkeletonBasedTkitsIT.class.getResource("/archive/test1_es.archive");
		File dir = new File(url.toURI()).getParentFile();

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".archive");
			}
		};
		return dir.list(filter);
	}
}
